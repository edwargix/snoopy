# CSCI-598 PVS Final Project - Snoopy

![snoopy](https://upload.wikimedia.org/wikipedia/en/5/53/Snoopy_Peanuts.png)

Snoop on your C functions to make sure they are allocating only what they need.

```bash
# apt install racket          # install Racket
$ cd path/to/snoopy/repo      # go to your cloned repo
$ raco pkg install            # install dependencies
$ racket main.rkt src_file.c  # snoop on functions in src_file.c
```

## Inner Workings

The following steps are taken to snoop on your functions.

- Using the [c-utils](https://pkgs.racket-lang.org/package/c-utils) package,
  Snoopy parses the provided C source code file to derive an abstract syntax
  tree (AST) of the C program.
- Using Racket's [pattern
  matching](https://docs.racket-lang.org/reference/match.html?q=match#%28form._%28%28lib._racket%2Fmatch..rkt%29._match%29%29),
  Snoopy converts the subtrees of the C AST corresponding to each C function
  into a Racket function that behaves very similarly to the C function but
  returns a vector of booleans where each boolean indicates whether a given
  variable was used.
- Using the amazing [Rosette](https://emina.github.io/rosette/) library, Snoopy
  ensures that every variable is used at least once on at least one set of
  inputs.  If no such set of inputs exists, the variable is deemed useless.
  Such useless variables are reported to the user.

## Current Limitations

- Only booleans and integers are currently allowed.
- Arrays are *almost* ready, but something's not working.  Thus, `example2.c`
  won't work yet.  Fixing this is first on my TODO list.
- Macros are allowed; however, if you `#include` header files, Snoopy will most
  likely crash because the Racket C parser does not support `__extension__`,
  which is present in the standard header files of most (all?) systems.
- Variable names can never be reused, even if the variables are in different
  scopes.
- Postfix operations (like `i++`) are not supported; only prefix operations
  (like `++i`) are.  This will be fixed very soon (I'm working on very little
  sleep right now).
- Code that uses a function argument in the test condition of a loop will
  greatly slow down Snoopy.
