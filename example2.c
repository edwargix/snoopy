#define BUFLEN 8

void use(int x) {}

void ex2(_Bool x)
{
	int a = 0;  // never used
	int buf[BUFLEN];

	for (int i = 0; i < BUFLEN; ++i) {
		++a;
		buf[i] = i % 10;
	}

	if (x)
		for (int j = 0; j < BUFLEN/2; ++j)
			use(buf[j]);
	else
		for (int k = BUFLEN/2; k < BUFLEN; ++k)
			use(buf[k]);
}
