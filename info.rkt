#lang info
(define collection "snoopy")
(define deps '("rosette" "c-utils"))
(define pkg-desc "Ensure your C functions use their variables")
(define version "0.0")
(define pkg-authors '(edwargix))

(define racket-launcher-names '("snoopy"))
(define racket-launcher-libraries '("main.rkt"))
