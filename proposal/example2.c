#include <limits.h>
#include <stdio.h>

#define BUFLEN 4096

int main(int argc, char *argv[])
{
	char buf[BUFLEN];
	int i;

	for (i = 0; i < BUFLEN; ++i)
		buf[i] = i % CHAR_MAX;

	for (i = 0; i < BUFLEN/2; ++i)
		printf("%d\n", buf[i]);

	return 0;
}
