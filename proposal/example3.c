#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

#define MAXLINE 1024

struct person {
	char name[MAXLINE];
	uint8_t age;
};

int main(int argc, char *argv[])
{
	struct person p;
	printf("Enter your name: \n");
	fgets(p.name, MAXLINE, stdin);
	printf("Hello, %s", p.name);
	return 0;
}
